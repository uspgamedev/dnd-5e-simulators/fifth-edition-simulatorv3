import glfw
import imgui
import OpenGL.GL as gl
from imgui.integrations.glfw import GlfwRenderer

from fesgui.menubar import menu
from fesgui.entrylist import EntryList


def impl_glfw_init():
    width, height = 800, 600
    window_name = "minimal ImGui/GLFW3 example"

    if not glfw.init():
        print("Could not initialize OpenGL context")
        exit(1)

    # OS X supports only forward-compatible core profiles from 3.2
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 0)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)

    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(
        int(width), int(height), window_name, None, None
    )
    glfw.make_context_current(window)

    if not window:
        glfw.terminate()
        print("Could not initialize Window")
        exit(1)

    return window


def main():
    # Initialize the library
    main_window = impl_glfw_init()
    impl = GlfwRenderer(main_window)
    windows = []

    # Loop until the user closes the main_window
    while not glfw.window_should_close(main_window):
        glfw.poll_events()
        impl.process_inputs()
        # Render here, e.g. using pyOpenGL
        # start new frame context
        imgui.new_frame()
        item_selected = menu()
        if item_selected is not None:
            windows.append(EntryList(item_selected))

        removed = []
        added = []
        for window in windows:
            result, new_window = window.process()
            if not result:
                removed.append(window)
            elif new_window is not None:
                added.append(new_window)
        for to_remove in removed:
            windows.remove(to_remove)
        for to_add in added:
            windows.append(to_add)

        gl.glClearColor(.1, .1, .1, 1)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        # pass all drawing comands to the rendering pipeline
        # and close frame context
        imgui.render()
        # Swap front and back buffers
        glfw.swap_buffers(main_window)

    impl.shutdown()
    imgui.shutdown()
    glfw.terminate()

if __name__ == "__main__":
    main()