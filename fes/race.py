
from fes.database import DB


class Race:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.traits = kwargs.get('traits')

    @staticmethod
    def db_name():
        return "races"

    @staticmethod
    def create(**kwargs):
        return Race(**kwargs)

    def apply_effects(self, char):
        for effect in self.traits:
            char.add_effect(effect)

