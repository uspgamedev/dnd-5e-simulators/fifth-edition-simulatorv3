
from random import choice
from fes.dice import D6
from fes.database import DB
from fes.character import Character
from fes.combat_statistics import CombatStatistics


class Combat:
    def __init__(self):
        self.combatants = []
        self.stats = {}

    def add_combatant(self, combatant):
        self.combatants.append(combatant)

    def play_round(self):
        for combatant in self.combatants:
            combatant.trigger('begin-turn')
            combatant.choose_action()
            combatant.execute_action(self.combatants)

            for maybe_dead in self.combatants:
                if maybe_dead.dead():
                    return

    def is_anyone_dead(self):
        for combatant in self.combatants:
            if combatant.dead():
                return True
        return False

    def run(self):
        initiatives = {combatant: combatant.roll_initiative()
                       for combatant in self.combatants}
        self.combatants = sorted(self.combatants, key=lambda x: initiatives[x],
                                 reverse=True)
        while not self.is_anyone_dead():
            self.play_round()
        self.award_winner()

    def award_winner(self):
        for combatant in self.combatants:
            if combatant.alive():
                combatant.won()
