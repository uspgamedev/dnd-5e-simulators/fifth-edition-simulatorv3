
class CombatStatistics:
    def __init__(self):
        self.wins = 0
        self.hits = 0
        self.damage = 0
        self.attacks = 0
        self.dodges = 0

    def add_data(self, data):
        if data['name'] == 'attack':
            self.attacks += 1
            if data.get('hit-success', False):
                self.hits += 1
                self.damage += data['damage-dealt']
        elif data['name'] == 'dodge':
            self.dodges += 1
        elif data['name'] == 'win':
            self.wins += 1

    def __add__(self, other):
        self.wins += other.wins
        self.hits += other.hits
        self.damage += other.damage
        self.attacks += other.attacks
        self.dodges += other.dodges
        return self

    def __radd__(self, other):
        return self + other
