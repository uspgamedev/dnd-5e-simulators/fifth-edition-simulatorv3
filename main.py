
import sys

from fes.database import DB
from fes.scenario import Scenario

def main(scenario_names):
    for name in scenario_names:
        treta = DB.get(Scenario, name)
        treta.run()
        treta.report()


if __name__ == '__main__':
    main(sys.argv[1:])
