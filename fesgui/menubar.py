
import imgui

DATABASE_ITEMS = ["Characters", "Races", "Classes", "Equipment"]


def menu():
    result = None
    if imgui.begin_main_menu_bar():
        if imgui.begin_menu("Menu", True):

            clicked_quit, selected_quit = imgui.menu_item(
                "Quit", 'Alt+F4'
            )

            if clicked_quit:
                exit(1)

            imgui.end_menu()
        if imgui.begin_menu("Database", True):
            for item_name in DATABASE_ITEMS:
                clicked, selected = imgui.menu_item(item_name)
                if clicked:
                    result = item_name
            imgui.end_menu()
        imgui.end_main_menu_bar()
    return result
