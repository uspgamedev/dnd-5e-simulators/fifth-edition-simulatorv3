
import imgui
from fes.database import DB
from fesgui.editors.equipment import EquipmentEditor

EDITORS = {
    'Equipment': EquipmentEditor
}


class EntryList:
    def __init__(self, table_name):
        self.table_name = table_name
        self.item_list = DB.all(table_name.lower())
        self.current = -1

    def process(self):
        expanded, opened = imgui.begin(self.table_name, True)
        new_window = None
        if expanded and opened:
            name_list = [item['name'] for item in self.item_list.values()] 
            clicked, current = imgui.listbox(self.table_name, self.current,
                                             name_list, 10)
            if clicked:
                self.current = current
            if imgui.button("New", 72):
                new_window = EDITORS[self.table_name]()
            if self.current >= 0:
                imgui.same_line(spacing=4)
                if imgui.button("Edit", 72):
                    pass
                imgui.same_line(spacing=4)
                if imgui.button("Remove", 72):
                    print("Not implemented yet, gomennasai senpai")
        # close current window context
        imgui.end()
        return opened, new_window
